from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.static import serve
from rest_framework.routers import SimpleRouter

from .api.stories import StoryViewset

api = SimpleRouter()
api.register("story", StoryViewset)

urlpatterns = [
    url(r'^api/', include(api.urls)),
    url(r'^story/', include('story.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^static/(?P<path>.*)$', serve, dict(
        document_root=settings.STATIC_ROOT,
        show_indexes=True
    )),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
