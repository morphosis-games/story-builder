
from rest_framework.serializers import ModelSerializer
from rest_framework.viewsets import ReadOnlyModelViewSet
from story.models import Story


class StorySerializer(ModelSerializer):
    class Meta(object):
        model = Story

    def to_representation(self, obj):
        return obj.to_dict()


class StoryViewset(ReadOnlyModelViewSet):
    serializer_class = StorySerializer
    queryset = Story.objects.all()
