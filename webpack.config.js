const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require("path");
const config = {
  context: path.join(__dirname, "/client/src"),
  node: {
    fs: "empty"
  },
  entry: "./app.js",
  output: {
    path: path.join(__dirname, "/client/www"),
    publicPath: "/",
    filename: "bundle.js"
  },
  resolve: {
    fallback: [
      path.join(__dirname, "client"),
      path.join(__dirname, "node_modules")
    ],
    extensions: ["", ".es6", ".js", ".nunjucks", ".html", ".md"]
  },
  resolveLoader: {
    fallback: [
      path.join(__dirname, "node_modules")
    ]
  },
  plugins: [
    new ExtractTextPlugin("bundle.css")
  ],
  module: {
    loaders: [
      {
        test: /\.es6$/,
        loader: "babel-loader",
        query: {
          presets: ['es2015']
        }
      },
      {
        test: /\.json$/,
        loader: "json-loader"
      },
      {
        test: /\.(nunjucks)$/,
        loader: "nunjucks-loader"
      },
      {
        test: /\.(less|css)$/,
        loader: ExtractTextPlugin.extract(
          "style-loader", "css-loader!less-loader")
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg)/,
        loader: "url-loader?limit=10000"
      },
      {
        test: /\.(woff|woff2)/,
        loader: "url-loader?limit=100000"
      },
      {
        test: /\.(ttf|eot)/,
        loader: "file-loader"
      },
      {
        test: /\.(wav|mp3)$/,
        loader: "file-loader"
      },
      {
        test: /\.(md|markdown)$/,
        loader: ["html-loader", "markdown-loader"]
      }
    ]
  },
  devServer: {
    contentBase: path.join(__dirname, "/client/www"),
    port: 4200
  },
  devtool: "#source-map"
};

module.exports = config;
