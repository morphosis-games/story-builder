from __future__ import print_function

import os
import sys
import time
import unittest
from os import path
from unittest import TestCase

from PIL import Image
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait as wait
from xvfbwrapper import Xvfb


class Device(object):

    def __init__(self, name, width, height, dpi=196, scale=1, pixel_scale=2):
        self.name = name
        self._width = width
        self._height = height
        self._dpi = dpi
        self.scale = scale
        self.driver = "Chrome"
        self.pixel_scale = pixel_scale

    @property
    def pixel_width(self):
        return self._width / self.pixel_scale

    @property
    def pixel_height(self):
        return self._height / self.pixel_scale + 100

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    @property
    def dpi(self):
        return self._dpi * self.scale

    def rotate(self):
        temp = self._width
        self._width = self._height
        self._height = temp
        return self


class ScreenshotTestCase(TestCase):
    browserDecHeight = 63
    screenshot_location = "screenshots/{}/{}.png"
    index_url = "http://localhost:4200/#/"
    devices = [
        Device(
            name="iphone-6-plus",
            width=1242,
            height=2208,
            dpi=288
        ).rotate(),
        Device(
            name="ipad-pro",
            width=2048,
            height=2732,
            dpi=196,
        ).rotate(),
    ]
    client = None

    def screenshot(self, device, name):
        filename = self.screenshot_location.format(
            device.name,
            name
        )
        try:
            os.makedirs(os.path.dirname(filename))
            print(" - created dir for image", filename)
        except OSError:
            pass
        print(" - saving screenshot", name)
        self.client.save_screenshot(filename)

        im = Image.open(filename)
        im.thumbnail((device.width, device.height + 200))
        im = im.crop((0, 0, device.width, device.height))
        im.save(filename)

    def testApp(self):
        for device in self.devices:
            self.device = device
            self.screen = Xvfb(width=3200, height=3200, dpi=str(device.dpi))
            if self.screen:
                self.screen.start()

            print("\nTesting", device.name, self.index_url)

            # webdriver.Opera
            self.client = getattr(webdriver, device.driver)()
            self.client.q = self.client.find_element_by_css_selector
            self.client.set_window_size(
                device.pixel_width,
                device.pixel_height + self.browserDecHeight,
            )

            # go to login page
            print(self.index_url)
            self.client.get(self.index_url)
            wait(self.client, 10).until(
                ec.presence_of_element_located(
                    (By.CSS_SELECTOR, "#id_1")
                )
            )
            time.sleep(5)

            # login
            self.screenshot(device, "index")
            self.client.q("#id_1").click()

            # show the shop
            time.sleep(30)
            self.screenshot(device, 'story')

            # done
            print(" - done\n")
            self.client.quit()
            self.client = None
            if self.screen:
                self.screen.stop()
            self.screen = None
            self.device = None

    def tearDown(self):
        super(ScreenshotTestCase, self).tearDown()
        try:
            if self.client:
                self.screenshot(self.device, "fail")
                for entry in self.client.get_log('browser'):
                    if not isinstance(entry['message'], dict):
                        print("{level}: {message}".format(**entry))
        except Exception as e:
            print(e)
        finally:
            if self.screen:
                self.screen.stop()
            if self.client:
                print(self.client)
                self.client.quit()


if __name__ == '__main__':
    unittest.main()
