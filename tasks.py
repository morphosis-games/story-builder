from __future__ import print_function

import os  # isort:skip
import sys  # isort:skip
import json # isort:skip
sys.path.append(os.path.abspath("."))  # isort:skip
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "story_builder.settings")  # isort:skip
import django; django.setup() # isort:skip
from django.conf import settings

import datetime
import logging
import sys
import time

import requests
from invoke import task as ctask, run, task


android_key = dict(
    location="ftt/",
    build_location="ftt/platforms/android/build/outputs/apk/",
    keystore_path="ftt/android.keystore",
    keystore_password="The cake is a lie",
    key_password="The cake is a lie",
    dist_location="dist/",
    alias="app",
)


def local(cmd, **kwargs):
    print(cmd)
    run(cmd, **kwargs)


@ctask()
def clearcache():
    from django.core.cache import cache
    cache.clear()


@ctask(pre=[])
def cordova():
    local("cd {location}; cordova run".format(**android_key))


archs = [
    # dict(
    #     name="crosswalk",
    #     prepare_commands=[
    #         "cd {location}; cordova plugin add cordova-plugin-crosswalk-webview --variable XWALK_MODE=\"lite\""
    #     ],
    #     build_flags="",
    # ),
    dict(
        name="normal",
        prepare_commands=[
            # "cd {location}; cordova plugin remove cordova-plugin-crosswalk-webview"
        ],
        build_flags=""  # "-- --gradleArg=-PcdvVersionCode=1080 --gradleArg=-PcdvMinSdkVersion=21"
    ),
]


@ctask(pre=[])
def build_android():
    for arch in archs:
        print(" ** building {}".format(arch['name']))
        android_key.update(arch)
        print(arch)
        local("cd {location}; cordova platform remove android".format(
            **android_key
        ), hide=True)
        local("cd {location}; cordova platform add android".format(
            **android_key
        ), hide=True)
        for cmd in arch.get("prepare_commands", []):
            local(cmd.format(**android_key), hide=True, warn=True)
        local("cd {location}; cordova build android --release {build_flags}".format(
            **android_key
        ), hide=True)
        print(" ** finished build")
        for filename in os.listdir(android_key.get("build_location")):
            print(" ** checking {}".format(filename))
            if filename.endswith("-release-unsigned.apk"):
                print("finishing: {}".format(filename))
                local(
                    "jarsigner -sigalg SHA1withRSA -digestalg SHA1 "
                    "-keystore {keystore_path} "
                    "-storepass \"{keystore_password}\" "
                    "-keypass \"{key_password}\" "
                    "{build_location}{filename} '{alias}'".format(
                        filename=filename,
                        **android_key),
                    hide=True
                )
                local(
                    "jarsigner -verify -certs "
                    "{build_location}{filename}".format(
                        filename=filename,
                        **android_key),
                    hide=True
                )
                local(
                    "zipalign -f 4 {build_location}{filename} "
                    "'{dist_location}{name}-{filename}'".format(
                        filename=filename,
                        **android_key),
                    hide=True
                )
        print(" ** completed {}".format(arch['name']))
    print(" ** completed all")


@ctask()
def preload_stories():
    response = requests.get("http://story.leithall.com/api/story/")
    # response = requests.get("http://localhost:8000/api/story/")
    with open("ftt/src/data/stories.json", "w") as fh:
        fh.write(response.text.encode("utf-8"))
