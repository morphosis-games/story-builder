var path = require('path')
var webpack = require('webpack')
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  entry: ['./src/styles/main.css', './src/main.js'],
  output: {
    path: path.resolve(__dirname, './www'),
    publicPath: '/',
    filename: 'build.js'
  },
  plugins: [
    new ExtractTextPlugin("build.css")
  ],
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            'css': ExtractTextPlugin.extract({loader: 'css-loader'})
          }
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({loader: 'css-loader'})
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      },
      {test: /\.(woff|woff2)/, loader: "url-loader?limit=100000"},
      {test: /\.(ttf|eot)/, loader: "file-loader"},
      {test: /\.(wav|mp3)$/, loader: "file-loader"},
    ]
  },
  devServer: {
    contentBase: "www",
    port: 4200,
    noInfo: true
  },
  performance: {
    hints: false
  },
  devtool: '#source-map'
};

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map';
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ]);
}
