// polyfills
import 'whatwg-fetch';

import Vue from 'vue';
import VueTouch from "vue-touch";
import Vuex from 'vuex';
import App from './App.vue';
import VueRouter from 'vue-router';
import ChooseStory from './components/choose_story.vue';
import ReadStory from './components/read_story.vue';
import GlobalMixin, {get, put} from "./globals";
import merge from "merge";

Vue.use(VueRouter);
Vue.use(VueTouch, {name: 'v-touch'});
Vue.use(Vuex);
Vue.mixin(GlobalMixin);

new Vue({
  el: '#app',
  store: new Vuex.Store({
    state: {
      playAudio: get("playAudio", true),
      stories: require("./data/stories.json"),
      sessions: get('sessions', {})
    },
    mutations: {
      save(state, opts) {
        state[opts.name] = merge(opts.data, state[opts.name])
      },
      resetKey(state, opts) {
        state[opts.name][opts.key] = opts.data;
      },
      reset(state, opts) {
        state[opts.name] = opts.data;
      }
    }
  }),
  router: new VueRouter({
    routes: [
      {path: '/', component: ChooseStory},
      {path: '/story/:id', component: ReadStory},
      {path: "*", component: {
        template: "",
        mounted() {
          this.$router.push("/");
        }
      }},
    ]
  }),
  render: h => h(App)
});
