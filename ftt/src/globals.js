const CODE = "story";
const SERVER = "https://www.lethall.com";


export function baseapi(url, params={}, method="GET") {
  console.warn(method, url, params);
  return fetch(`${SERVER}${url}`, {
    method: method,
    body: new FormData(params),
    headers: {
      Authorization: `Token ${TOKEN}`
    }
  }).then(function(response) {
    return response.json();
  });
}

export function api(url, params={}, method="GET") {
  return baseapi(`/api/v1/rest/${CODE}${url}`, params, method);
}

export function get(key, def) {
  try {
    let retval = JSON.parse(localStorage[`${CODE}_${key}`]) || def;
    return retval;
  } catch(ex) {
    console.log("failed to get from localStorage", `${CODE}_${key}`, localStorage[`${CODE}_${key}`]);
    return def;
  }
}

export function put(key, value) {
  let savedValue = JSON.stringify(value);
  localStorage[`${CODE}_${key}`] = savedValue;
}

export function scan() {
  if (window.cordova) {
    return new Promise((resolve, reject) => {
      cordova.plugins.barcodeScanner.scan((result) => {
        console.log("success", result);
        if (result.cancelled) {
          reject(new Error("cancelled"));
        } else {
          resolve(result.text);
        }
      }, (error) => {
        console.log("error", error);
        reject(error);
      });
    });
  } else {
    return new Promise((resolve) => {
      resolve(prompt("Type scan result here:"));
    });
  }
}

export function camera() {
  if (window.cordova) {
    return new Promise((resolve, reject) => {
      navigator.camera.getPicture((result) => {
        console.log("success", result);
        if (result.cancelled) {
          reject(new Error("cancelled"));
        } else {
          resolve(result);
        }
      }, (error) => {
        console.log("error", error);
        reject(error);
      });
    });
  } else {
    return new Promise((resolve) => {
      resolve(prompt("Type image url here:"));
    });
  }
}

export default {
  methods: {
    get, put, baseapi, api
  }
}