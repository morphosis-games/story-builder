# Story builder

helps writers create a .json file for releasing the story for app consumption

# TODO
  - [x] set first scene on story
  - [ ] do story checks
    - [ ] check for story nodes that don't go anywhere
    - [ ] check for attributes that aren't ever read  

  - [ ] record statistics
    - [ ] show them at the end of the game

  - make savepoints, where players can revert to, it can be a consequence
  - show select character stats at the end of the game

## Web Components

### story-container
 - [x] can load json
 - [x] Shows the starting scene
 - [x] resumes the scene from the session if any
 - [x] handles the progression of scenes

### story-scene
 - [x] can load json for a scene
 - [x] The background is a static image that is not intended to change
 - [x] show the current text
 - [x] shows controls to go forward or backward between content
 - [x] first content only has controls to go forward
 - [x] last content has a back button and also shows all choices
 - [x] choices can be filtered by a player trait
 - [ ] show ending when there are no choices

### story-player
 - [ ] listens for player events
 - [ ] persists player data
